import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MobileLoanComponent } from './mobile-loan.component';

describe('MobileLoanComponent', () => {
  let component: MobileLoanComponent;
  let fixture: ComponentFixture<MobileLoanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MobileLoanComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MobileLoanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
