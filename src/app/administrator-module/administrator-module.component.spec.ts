import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdministratorModuleComponent } from './administrator-module.component';

describe('AdministratorModuleComponent', () => {
  let component: AdministratorModuleComponent;
  let fixture: ComponentFixture<AdministratorModuleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdministratorModuleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdministratorModuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
