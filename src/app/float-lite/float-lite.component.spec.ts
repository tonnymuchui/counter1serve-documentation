import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FloatLiteComponent } from './float-lite.component';

describe('FloatLiteComponent', () => {
  let component: FloatLiteComponent;
  let fixture: ComponentFixture<FloatLiteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FloatLiteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FloatLiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
