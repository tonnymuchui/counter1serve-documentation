import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BackOfficeLoansComponent } from './back-office-loans.component';

describe('BackOfficeLoansComponent', () => {
  let component: BackOfficeLoansComponent;
  let fixture: ComponentFixture<BackOfficeLoansComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BackOfficeLoansComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BackOfficeLoansComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
