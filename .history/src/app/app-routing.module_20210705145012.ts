import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import {CustomerComponent} from './customer/customer.component';
import { MobileLoanComponent } from './mobile-loan/mobile-loan.component';
import { BackOfficeLoansComponent } from './back-office-loans/back-office-loans.component';
import { ApprovalsComponent } from './approvals/approvals.component';
import { SavingsComponent } from './savings/savings.component';
import { AssetsComponent } from './assets/assets.component';
import { FloatLiteComponent } from './float-lite/float-lite.component';


const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'customers',component: CustomerComponent },
  { path: 'mobile-loan',component: MobileLoanComponent },
  { path: 'back-office-loans',component: BackOfficeLoansComponent },
  { path: 'approvals',component: ApprovalsComponent },
  { path: 'savings',component: SavingsComponent },
  { path: 'assets',component: AssetsComponent },
  { path: 'floats',component: FloatLiteComponent },
  { path: 'floats',component: FloatLiteComponent },



  { path: 'user-pages', loadChildren: () => import('./user-pages/user-pages.module').then(m => m.UserPagesModule) },
  { path: 'error-pages', loadChildren: () => import('./error-pages/error-pages.module').then(m => m.ErrorPagesModule) },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
