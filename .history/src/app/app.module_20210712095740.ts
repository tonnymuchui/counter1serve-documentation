import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ChartsModule, ThemeService } from 'ng2-charts';

import { AppComponent } from './app.component';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { SidebarComponent } from './shared/sidebar/sidebar.component';
import { FooterComponent } from './shared/footer/footer.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ContentAnimateDirective } from './shared/directives/content-animate.directive';
import { TodoListComponent } from './apps/todo-list/todo-list.component';
import { CustomerComponent } from './customer/customer.component';
import { MobileLoanComponent } from './mobile-loan/mobile-loan.component';
import { BackOfficeLoansComponent } from './back-office-loans/back-office-loans.component';
import { ApprovalsComponent } from './approvals/approvals.component';
import { SavingsComponent } from './savings/savings.component';
import { AssetsComponent } from './assets/assets.component';
import { FloatLiteComponent } from './float-lite/float-lite.component';
import { ReceiptsComponent } from './receipts/receipts.component';
import { ReportsComponent } from './reports/reports.component';
import { AllReportsComponent } from './all-reports/all-reports.component';
import { InquiriesComponent } from './inquiries/inquiries.component';
import { AdministratorModuleComponent } from './administrator-module/administrator-module.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    SidebarComponent,
    FooterComponent,
    DashboardComponent,
    TodoListComponent,
    TodoComponent,
    ContentAnimateDirective,
    CustomerComponent,
    MobileLoanComponent,
    BackOfficeLoansComponent,
    ApprovalsComponent,
    SavingsComponent,
    AssetsComponent,
    FloatLiteComponent,
    ReceiptsComponent,
    ReportsComponent,
    AllReportsComponent,
    InquiriesComponent,
    AdministratorModuleComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    ChartsModule,
  ],
  providers: [ThemeService],
  bootstrap: [AppComponent]
})
export class AppModule { }
